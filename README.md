# Common Util BOM

This BOM represents dependencies common dependencies used by FRC team 3786 for development.

## Usage

In build.gradle of the project that needs the BOM:

* Update the `repositories` closure to include the requisite Maven repository and the repository of any dependency that you are looking to use:
```gradle
repositories {
    ...
    maven {
        url = "https://gitlab.com/api/v4/groups/chargerrobotics3786/-/packages/maven"
        name = "Charger Robotics Common"
    }
    mavenCentral()
    ...
}
```

* Add a platform implementation for the BOM along with the implementation of any dependency you want to use to the `dependencies` closure
```gradle
dependencies {
    ...
    implementation platform('com.chargerrobotics.common:common-util-bom:<BOM_VERSION>')

    implementation "org.slf4j:slf4j-api"
}
```
